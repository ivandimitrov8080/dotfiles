# Set variables
export EDITOR=nvim
export TZ='Europe/Sofia'
export PNPM_HOME="/home/ivand/.local/share/pnpm"
export NVM_DIR="$HOME/.nvm"

# Set sources

source <(gopass completion zsh) > /dev/null 2>&1
source /usr/share/git/completion/git-completion.zsh > /dev/null 2>&1
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion


# Set aliases
alias config='/usr/bin/git --git-dir=$HOME/.cfg/ --work-tree=$HOME'
alias nv='nvim'
alias torrent='transmission-remote'
alias mutt='neomutt'

# Define functions

### --- FFMPEG FUNCTIONS --- ###
trim() {
	ffmpeg -i "$1" -ss "$2" -to "$3" -c:v copy -c:a copy "$4"
}

compress() {
	ffmpeg -i "$1" -vcodec libx265 -crf 28 "$2"
}

# https://stackoverflow.com/a/36324719
audio() {
	ffmpeg -i "$1" -q:a 0 -map a "$2"
}

### --- ISO FUNCTIONS --- ###
etch() {
	sudo dd if="$1" of="$2" bs=4M conv=fdatasync status=progress
}

